package com.knf.dev.demo.crudapplication.entity;

import jakarta.persistence.*;


@Entity
@Table(name = "produit")
public class Produit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "nom")
    private String nom;
    @Column(name = "prix")
    private double prix;
    @Column(name = "quantite")
    private double quantite;

    public Produit() {
    }

    public Produit(String nom, Double prix, Double quantite) {
        super();
        this.nom = nom;
        this.prix = prix;
        this.quantite = quantite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getnom() {
        return nom;
    }

    public void setnom(String nom) {
        this.nom = nom;
    }

    public double getprix() {
        return prix;
    }

    public void setprix(Double prix) {
        this.prix = prix;
    }

    public double getquantite() {
        return quantite;
    }

    public void setquantite(Double quantite) {
        this.quantite = quantite;
    }
}