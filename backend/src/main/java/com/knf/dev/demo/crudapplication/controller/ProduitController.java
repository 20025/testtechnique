package com.knf.dev.demo.crudapplication.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.knf.dev.demo.crudapplication.entity.Produit;
import com.knf.dev.demo.crudapplication.exception.ResourceNotFoundException;
import com.knf.dev.demo.crudapplication.repository.ProduitRepository;
import com.knf.dev.demo.crudapplication.repository.ProduitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class ProduitController {
    @Autowired
    private ProduitRepository ProduitRepository;

    // get all Produits
    @GetMapping("/produit")
    public List<Produit> getAllProduits() {
        return ProduitRepository.findAll();
    }

    // create Produit rest API
    @PostMapping("/produit")
    public Produit createProduit(@RequestBody Produit Produit) {
        return ProduitRepository.save(Produit);
    }

    // get Produit by id rest api
    @GetMapping("/produit/{id}")
    public ResponseEntity<Produit> getProduitById(@PathVariable Long id) {
        Produit Produit = ProduitRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException
                      ("Produit not exist with id :" + id));
        return ResponseEntity.ok(Produit);
    }

    // update Produit rest api
    @PutMapping("/produit/{id}")
    public ResponseEntity<Produit> updateProduit(@PathVariable Long id,
             @RequestBody Produit ProduitDetails) {
        Produit Produit = ProduitRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException
                      ("Produit not exist with id :" + id));
        Produit.setnom(ProduitDetails.getnom());
        Produit.setprix(ProduitDetails.getprix());
        Produit.setquantite(ProduitDetails.getquantite());
        Produit updatedProduit = ProduitRepository.save(Produit);
        return ResponseEntity.ok(updatedProduit);
    }

    // delete Produit rest api
    @DeleteMapping("/produit/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteProduit
               (@PathVariable Long id) {
        Produit Produit = ProduitRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException
            ("Produit not exist with id :" + id));
        ProduitRepository.delete(Produit);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return ResponseEntity.ok(response);
    }
}
