import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/service/crud.service';

@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.scss']
})
export class RechercheComponent  {

  searchQuery: string | undefined;
  searchResults: any[] = [];
  updateForm: any;
  produit: any = [];

  constructor(private crudService: CrudService) {}
  delete(id: any, i: any) {
    console.log(id);
    if (window.confirm('Do you want to go ahead?')) {
      this.crudService.deleteUser(id).subscribe(() => {
        this.produit.splice(1, 1);
      });
    }
  }
  onSubmit() {
    this.crudService.GetProduitNom(this.searchQuery).subscribe((res: { [x: string]: any; }) => {
      this.produit = res;
    });
  }
}