export class Produit {
  id!: number;
  nom!: String;
  prix!: number;
  quantite!: number;
}
